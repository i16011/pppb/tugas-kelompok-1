package net.christianto.unpar.pppb.pppb1170.Engine;

import android.graphics.Point;
import android.graphics.Rect;

import net.christianto.unpar.pppb.pppb1170.Engine.Internal.RandomString;
import net.christianto.unpar.pppb.pppb1170.Engine.Models.CircleObstacle;
import net.christianto.unpar.pppb.pppb1170.Engine.Models.Obstacle;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Will provide randomizer for the game obstacle.
 */
public class GachaRandomizer {
    private static final String LOGTAG = "GACHAPON";
    private int[] colors;
    public static int colorCombi = 3;
    public static int textCombi = 2;
    public static TextRandomizeQuality quality = TextRandomizeQuality.EASY;
    public static boolean randomizeColor = true;
    public static boolean randomizeShape = true;

    public enum TextRandomizeQuality {
        EASY, // Kambing Capybara
        INTERMEDIATE, // KAMBING KUMBANG
        HARD, // kevin kelvin
        UNSOLVEABLE, //AxSyDER AxSslDER
    }

    public GachaRandomizer(){
        this.colors= new int[0];
    }

    public String[] getRandomText(TextRandomizeQuality quality, int length) {
        int biasa = 5;
        if(quality == TextRandomizeQuality.HARD || quality == TextRandomizeQuality.UNSOLVEABLE) {
            biasa = 3;
        }
        RandomString randomizer = new RandomString(biasa);

        String[] hasil = new String[length];
        String prev = null;
        for(int i=0; i<length; i++) {
            hasil[i] = randomizer.nextString();
            if(prev == null) {
                prev = hasil[i].substring(0,3);
            }
            if(quality == TextRandomizeQuality.EASY){
                hasil[i] = hasil[i].toLowerCase();
            } else if(quality == TextRandomizeQuality.INTERMEDIATE){
                hasil[i] = hasil[i].toUpperCase();
            } else if(quality == TextRandomizeQuality.HARD){
                hasil[i] = prev.toUpperCase() + hasil[i].toUpperCase() + prev.toUpperCase();
            } else if(quality == TextRandomizeQuality.UNSOLVEABLE){
                hasil[i] = prev + hasil[i] + prev;
            }
        }
        return hasil;
    }

    public String[] getRandomText(){
        return getRandomText(quality, textCombi);
    }

    /**
     * Will generate random color
     * @return
     */
    public int getRandomColor(){
        if(!randomizeColor)
            return colors[1];
        SecureRandom rd = new SecureRandom();
        int index = rd.nextInt(colors.length);
        return colors[index];
    }

    /**
     * Set colors bank for the game.
     * @param arr
     */
    public void setColors(int[] arr){
        this.colors=arr;
    }


    public Obstacle.Groups generateObstableWithDesiredDificulty(Rect drawableBoundary) {
        Obstacle.Groups grup = new Obstacle.Groups();
        int[] colors = new int[colorCombi];
        String[] texts = getRandomText();

        //obstacleType== 1 -> kotak, obstacle Type == 0 lingkaran
        Random rand = new Random();
        int obstacleType = rand.nextInt(2);

        ArrayList<Obstacle> obstacle = new ArrayList<>();
        float maxMarginW=0, maxMarginH=0;
        for(int i=0; i<colorCombi; i++){
            colors[i] = getRandomColor();
            for(int j=0; j<texts.length; j++) {
                Obstacle kambing;
                if(obstacleType==0 && randomizeShape) {
                    kambing = new CircleObstacle(colors[i], texts[j], getRandomControlledCoordinateFromParameter(drawableBoundary, maxMarginW, maxMarginH));
                }else {
                    kambing = new Obstacle(colors[i], texts[j], getRandomControlledCoordinateFromParameter(drawableBoundary, maxMarginW, maxMarginH));
                }
                maxMarginH = Math.max(maxMarginH, kambing.getObstacleAsRectangle().height());
                maxMarginW = Math.max(maxMarginW, kambing.getObstacleAsRectangle().width());
                obstacleType=rand.nextInt(2);
                int letgo=0;
                while(isObstacleCollideEachOther(obstacle, kambing) && letgo++ <= 12){
                    kambing.setPos(getRandomControlledCoordinateFromParameter(drawableBoundary,maxMarginW, maxMarginH));
                    if(letgo >= 10 && isObstacleCollideEachOther(obstacle, kambing)) {
                        //kita coba geser aja.
                        kambing.setPosX(kambing.getPosX()-kambing.getObstacleAsRectangle().width()-10);

                        if(kambing.getPosY() > drawableBoundary.height()) {
                            kambing.setPosY(kambing.getPosY() - kambing.getObstacleAsRectangle().height() - 10);
                        }
                    }
                }
                obstacle.add(kambing);
            }
        }

        SecureRandom sr = new SecureRandom();

        Obstacle answer = obstacle.get(sr.nextInt(obstacle.size()));
        answer.setAnswer(true);

        grup.obstacles = obstacle.toArray(new Obstacle[0]);
        grup.challenge = answer;

        //finally
        return grup;
    }

    SecureRandom sr = new SecureRandom();
    public Point getRandomControlledCoordinateFromParameter(Rect boundary, float maxMarginW, float maxMarginH) {
        Point res = new Point();
        int x = sr.nextInt(boundary.width()-((int)maxMarginW+30));
        int y = sr.nextInt(boundary.height()-((int)maxMarginH+30));

        res.set(x,y);
        return res;
    }

    public boolean isObstacleCollideEachOther(ArrayList<Obstacle> obs, Obstacle main) {
        Iterator<Obstacle> itKambing = obs.iterator();
        while (itKambing.hasNext()) {
            if(itKambing.next().isColliding(main))
                return true;
        }
        return false;
    }
}
