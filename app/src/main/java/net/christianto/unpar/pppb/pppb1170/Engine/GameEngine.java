package net.christianto.unpar.pppb.pppb1170.Engine;

import net.christianto.unpar.pppb.pppb1170.Engine.Models.Obstacle;

import java.util.ArrayList;

public interface GameEngine {

    Obstacle[] getObstacles();
    Obstacle getChallengeObstacle();

    long getHighScore();
    long getScore();
    ArrayList<Long> getScores();

    GameStatus getGameStatus();

    void triggerGameStart();
    void triggerGameStop();
    void triggerGamePause();
    void triggerGameResume();

    void onTouched(float x, float y);

    void setGameSetting(int height, int width);

    void setGameDifficulty(GameDifficulty difficulty);

    void setColorChoices(int[] arrColor);

    /**
     * Returns current nyawa total.
     * @return
     */
    int getNyawa();

    void gameBoardReady();
}
