package net.christianto.unpar.pppb.pppb1170.Engine;

public enum GameDifficulty {
    EASY,
    INTERMEDIATE,
    HARD,
    UNSOLVEABLE,
    PROGRESSING
}
