package net.christianto.unpar.pppb.pppb1170.Engine.Models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.drawable.shapes.ArcShape;
import android.util.Log;

import net.christianto.unpar.pppb.pppb1170.Engine.Models.Obstacle;

public class CircleObstacle extends Obstacle {
    private static final String LOGTAG = "Circle Obstacle";

    public CircleObstacle(int color, String text, float posX, float posY){
        super(color,text,posX,posY);
    }

    public CircleObstacle(int color,String text,Point position){
        super(color,text,position.x,position.y);
    }

    protected void updateBoundary() {
        tp.getTextBounds(this.text, 0, text.length(), textBounder);
        float totalWidth = textBounder.width() + padding*2;

        boundary = new RectF(posX, posY, posX + totalWidth, posY + totalWidth);
    }

    public void drawToImageView(Canvas cnv){
        Paint akatsuki = new Paint();
        akatsuki.setAntiAlias(true);
        akatsuki.setColor(this.getColor());
        cnv.drawArc(boundary, 0,360,true,akatsuki);

        float textX = posX+padding;
        float textY = posY+boundary.width()/2-textBounder.height()/2+textBounder.height();
        cnv.drawText(text, textX, textY, tp);
    }

    public void drawItInChallengeBox(Canvas cnv) {
        Paint akatsuki = new Paint();
        akatsuki.setAntiAlias(true);
        akatsuki.setColor(color);

        float left = (cnv.getWidth()/2),
                top  = (cnv.getHeight()/2);

        float radius = textBounder.width()/2 + padding;

        cnv.drawCircle(left, top, radius, akatsuki);
        cnv.drawText(text, left-(textBounder.width()/2), top + (textBounder.height()/2), tp);
    }
}
