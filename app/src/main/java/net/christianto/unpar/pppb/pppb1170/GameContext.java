package net.christianto.unpar.pppb.pppb1170;

interface GameContext {
    void enterGame();
    void enterScoreBoard();

    void enterMainMenu();
    void enterSetting();
}
