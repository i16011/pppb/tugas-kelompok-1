package net.christianto.unpar.pppb.pppb1170.Engine;

public enum GameStatus {
    RUNNING,
    ENDED,
    PAUSED
}
