package net.christianto.unpar.pppb.pppb1170.Helper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.christianto.unpar.pppb.pppb1170.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ScoreAdapter extends BaseAdapter {
    ArrayList<Long> scores;
    private LayoutInflater layoutInflater;

    public static ScoreAdapter createInstance(LayoutInflater inflater, ArrayList<Long> scores) {
        ScoreAdapter kambing = new ScoreAdapter();
        kambing.layoutInflater = inflater;
        kambing.scores = scores;
        return kambing;
    }
    @Override
    public int getCount() {
        return scores.size();
    }

    @Override
    public Object getItem(int position) {
        return scores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View kambin = layoutInflater.inflate(R.layout.layout_scores, null, false);
        TextView lblScore = kambin.findViewById(R.id.lblScore);
        lblScore.setText(String.format("%01d", scores.get(position)));
        return kambin;
    }
}
