package net.christianto.unpar.pppb.pppb1170;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.christianto.unpar.pppb.pppb1170.Engine.Core;
import net.christianto.unpar.pppb.pppb1170.Engine.GameBoard;
import net.christianto.unpar.pppb.pppb1170.Engine.GameEngine;
import net.christianto.unpar.pppb.pppb1170.Engine.GameStatus;
import net.christianto.unpar.pppb.pppb1170.Engine.GameTicksEvent;
import net.christianto.unpar.pppb.pppb1170.Engine.Models.Obstacle;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements GameBoard, GameContext {

    private static final String LOGTAG = "MainAPP";

    GameEngine ge;

    FragmentGameplay fgame;
    FragmentScore fscore;
    FragmentScoreboard fboard;
    FragmentGameSetting fsetting;
    FragmentGameStart fstart;

    FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ge = new Core(this, this.getApplicationContext());



        fgame = FragmentGameplay.createInstance(ge, this);
        fscore = FragmentScore.createInstance(ge, this);
        fboard = FragmentScoreboard.createInstance(ge,this);
        fsetting = FragmentGameSetting.createInstance(ge, this);
        fstart = FragmentGameStart.createInstance(ge,this);
        fm = this.getSupportFragmentManager();
        enterMainMenu();
    }

    @Override
    public void onInitialization() {

    }

    @Override
    public void onGameStart() {
        // Pindah ke fragment gameplay
        fm.beginTransaction().replace(R.id.lytContainer,fgame).commit();
    }

    @Override
    public void onGameEnd() {
        // pindah ke fragment score.
        fm.beginTransaction().replace(R.id.lytContainer,fscore).commit();
    }

    @Override
    public void onGamePaused() {
        //not yet implemented. Asalnya buat nyimpen benda benda.
    }

    @Override
    public void onGameResumed() {
        //not yet implemented. Asalnya buat balikin benda benda.
    }

    @Override
    public void onGameTicks(GameTicksEvent ticksEvent) {
        Log.d(LOGTAG, "Game ticks!");
        fgame.onGameTicks(ticksEvent);
    }


    @Override
    public void enterGame() {
        ge.triggerGameStart();
    }

    @Override
    public void enterScoreBoard() {
        fm.beginTransaction().replace(R.id.lytContainer,fboard).commit();

    }

    @Override
    public void enterMainMenu() {
        fm.beginTransaction().replace(R.id.lytContainer,fstart).commit();

    }

    @Override
    public void enterSetting() {
        fm.beginTransaction().replace(R.id.lytContainer,fsetting).commit();

    }
}
