package net.christianto.unpar.pppb.pppb1170;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import net.christianto.unpar.pppb.pppb1170.Engine.GameEngine;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGameStart extends Fragment implements View.OnTouchListener{


    private GameEngine gameEngine;
    private GameContext gameContext;

    Button btnStart, btnSetting, btnScoreBoard;

    public FragmentGameStart() {
        // Required empty public constructor
    }

    public static FragmentGameStart createInstance(GameEngine engine, GameContext gameContext) {
        FragmentGameStart korban = new FragmentGameStart();
        korban.gameEngine = engine;
        korban.gameContext = gameContext;
        return korban;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_game_start, container, false);
        btnScoreBoard = view.findViewById(R.id.btn_scoreboard);
        btnStart = view.findViewById(R.id.btn_start);
        btnSetting = view.findViewById(R.id.btn_settings);

        btnScoreBoard.setOnTouchListener(this);
        btnStart.setOnTouchListener(this);
        btnSetting.setOnTouchListener(this);
        return view;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v == btnScoreBoard){
            gameContext.enterScoreBoard();
        } else if (v==btnSetting) {
            gameContext.enterSetting();
        } else if (v==btnStart){
            gameContext.enterGame();
        }
        return true;
    }
}
