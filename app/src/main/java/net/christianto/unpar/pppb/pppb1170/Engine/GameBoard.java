package net.christianto.unpar.pppb.pppb1170.Engine;

public interface GameBoard {
    /**
     * Provide events for initializing game API and such.
     *
     */
    void onInitialization();

    /**
     * Provide events for game to start.
     * Should reposisionate all game and so much other.
     */
    void onGameStart();

    /**
     * Provide events for game to stop.
     * Should... idk what to do.
     */
    void onGameEnd();

    /**
     * Buat nampilin banyak benda dkk
     */
    void onGamePaused();

    /**
     * Buat nampilin banyak benda dkk
     */
    void onGameResumed();

    /**
     * Provide some interesting events on game.
     */
    void onGameTicks(GameTicksEvent ticksEvent);
}
