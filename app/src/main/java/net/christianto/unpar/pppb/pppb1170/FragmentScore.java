package net.christianto.unpar.pppb.pppb1170;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.christianto.unpar.pppb.pppb1170.Engine.GameEngine;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentScore extends Fragment implements View.OnClickListener{


    private GameEngine gameEngine;
    private GameContext gameContext;

    public FragmentScore() {
        // Required empty public constructor
    }

    public static FragmentScore createInstance(GameEngine engine, GameContext gameContext) {
        FragmentScore korban = new FragmentScore();
        korban.gameEngine = engine;
        korban.gameContext = gameContext;

        return korban;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_fragment_score, container, false);

        TextView lblScore = view.findViewById(R.id.lblScore);

        lblScore.setText(String.format("%01d", gameEngine.getScore()));

        Button btnMainMenu = view.findViewById(R.id.btnMainMenu);
        btnMainMenu.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
       gameContext.enterMainMenu();
    }
}
