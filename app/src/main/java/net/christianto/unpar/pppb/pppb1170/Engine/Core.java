package net.christianto.unpar.pppb.pppb1170.Engine;

import android.content.Context;
import android.graphics.Rect;
import android.util.Log;

import net.christianto.unpar.pppb.pppb1170.Engine.Models.Obstacle;

import java.util.ArrayList;

/**
 * Game enginge core. Will handle all sorts of many things.
 */
public class Core implements GameEngine {

    private static final String LOGTAG = "Engine";
    private static final int DEFAULT_NYAWA = 3;
    private GameBoard gameBoard;
    private Informan informan;
    private GachaRandomizer randomizer;
    private Rect screenSize;

    private GameStatus gameStatus = GameStatus.ENDED;
    private boolean hasGameInitialized = false;

    private int nyawa = 3;

    /**
     * PER STAGE
     */
    Obstacle.Groups obstacles;


    public Core(GameBoard gameBoard, Context appContext) {
        this.gameBoard = gameBoard;
        this.informan = Informan.getInstance(appContext);
        this.randomizer = new GachaRandomizer();
    }

    @Override
    public Obstacle[] getObstacles() {
        if (obstacles == null)
            return null;
        return obstacles.obstacles;
    }

    @Override
    public Obstacle getChallengeObstacle() {
        if (obstacles == null)
            return null;
        return obstacles.challenge;
    }

    @Override
    public long getHighScore() {
        return this.informan.getHighscore();
    }

    @Override
    public long getScore() {
        return this.informan.getScore();
    }

    @Override
    public ArrayList<Long> getScores() {
        return informan.getScores();
    }

    @Override
    public GameStatus getGameStatus() {
        return gameStatus;
    }

    @Override
    public void triggerGameStart() {
        if(!hasGameInitialized) {
            Log.d(LOGTAG, "Game have not initialized, initializing.");
            gameBoard.onInitialization();
        }
        Log.d(LOGTAG,"Game start tiggered.");
        gameStatus = GameStatus.RUNNING;
        gameBoard.onGameStart();
        informan.resetScore();

        nyawa = DEFAULT_NYAWA;

    }

    @Override
    public void triggerGameStop() {
        gameStatus = GameStatus.ENDED;

        informan.setScoreEnd();
        gameBoard.onGameTicks(GameTicksEvent.SCORE);

        gameBoard.onGameEnd();
    }

    @Override
    public void triggerGameResume() {
        gameStatus = GameStatus.RUNNING;
        gameBoard.onGameResumed();
    }

    @Override
    public void triggerGamePause() {
        gameStatus = GameStatus.PAUSED;
        gameBoard.onGamePaused();
    }

    protected Obstacle getObstacleInRange(float x, float y) {
        for (Obstacle ob : obstacles.obstacles) {
            Log.d(LOGTAG, "Checking " + ob.toString());
            if (ob.isReallyIsTouched(x, y)) {
                Log.d(LOGTAG, "Found!");
                return ob;
            }
        }
        return null;
    }

    @Override
    public void onTouched(float x, float y) {
        Log.d(LOGTAG, "Information recieved with x:" + x + " and y:" + y);
        Obstacle selectedObstacle = getObstacleInRange(x, y);
        Log.d(LOGTAG, "Obstacle detected...:");
        if (selectedObstacle == null) {
            Log.d(LOGTAG, "No Obstacle detected.");
            return;
        }

        Log.i(LOGTAG, "Congrats! An object found:" + selectedObstacle.toString());

        if(selectedObstacle.getAnswer()) {
            Log.i(LOGTAG, "POINT! You hit the correct one.");
            informan.bumpsUpScore(100);
        } else {
            Log.i(LOGTAG, "WRONG! Minus point!");
            informan.bumpsUpScore(-20);
            nyawa--;
        }

        gameBoard.onGameTicks(GameTicksEvent.SCORE);

        if(nyawa<=0) {
            triggerGameStop();
            return; // no need to trigger new stage.
        }

        triggerPrepareNewStage();
    }

    @Override
    public void setGameSetting(int height, int width) {
        Log.d(LOGTAG, "Set game boundary: " + height + " ## " + width);
        screenSize = new Rect(0, 0, width, height);
    }

    @Override
    public void setGameDifficulty(GameDifficulty difficulty) {

        // TODO: ini buat set kesulitangame, biasanya buat kasih shape yang bervariasi, warna yang mirip-mirip, ato yang lain lain.
        // Game documentation bakal ngupdate parameter yang ada di randomizer, restart game.
    }

    @Override
    public void setColorChoices(int[] arrColor) {
        this.randomizer.setColors(arrColor);
    }

    @Override
    public int getNyawa() {
        return nyawa;
    }

    @Override
    public void gameBoardReady() {
        triggerPrepareNewStage();
    }

    /**
     * Will trigger to create new stage and finally call the respected events.
     * Stages of preparation:
     * - Create new randomized object
     * - Set them to attribute
     * - Trigger game ticks.
     */
    protected void triggerPrepareNewStage() {
        Log.d(LOGTAG, "Preparement starting");
        this.obstacles = randomizer.generateObstableWithDesiredDificulty(screenSize);
        Log.d(LOGTAG, "Randomization done, triggering the ticks");
        gameBoard.onGameTicks(GameTicksEvent.OBSTACLES);
        gameBoard.onGameTicks(GameTicksEvent.NYAWA);
        Log.d(LOGTAG, "Ticked.");
    }


}
