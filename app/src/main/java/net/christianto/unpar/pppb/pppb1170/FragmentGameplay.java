package net.christianto.unpar.pppb.pppb1170;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.christianto.unpar.pppb.pppb1170.Engine.Core;
import net.christianto.unpar.pppb.pppb1170.Engine.GameEngine;
import net.christianto.unpar.pppb.pppb1170.Engine.GameStatus;
import net.christianto.unpar.pppb.pppb1170.Engine.GameTicksEvent;
import net.christianto.unpar.pppb.pppb1170.Engine.Models.Obstacle;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGameplay extends Fragment implements View.OnTouchListener, View.OnClickListener {


    private static final String LOGTAG = "Gameplay F";
    private GameEngine gameEngine;
    private GameContext gameContext;

    Button btnAction;
    ImageView imgGame,imgAnswer;
    Canvas cnvKambing,cnvAnswer;
    Bitmap bmpKambing,bmpAnswer;

    TextView lblNyawa, lblScore, lblHigh;

    GestureDetector gesture;


    public FragmentGameplay() {
        // Required empty public constructor
    }

    public static FragmentGameplay createInstance(GameEngine engine, GameContext gameContext) {
        FragmentGameplay korban = new FragmentGameplay();
        korban.gameEngine = engine;
        korban.gameContext = gameContext;

        return korban;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_fragment_gameplay, container, false);

        lblHigh = view.findViewById(R.id.label_high_score);
        lblScore = view.findViewById(R.id.label_score);

        // initialization
        imgGame = view.findViewById(R.id.ivMain);
        imgAnswer = view.findViewById(R.id.ivAnswer);

        imgGame.setOnTouchListener(this);

        btnAction = view.findViewById(R.id.btnAction);
        btnAction.setOnClickListener(this);

        lblNyawa = view.findViewById(R.id.label_nyawa);

        gesture = new GestureDetector(view.getContext(), new SimpleGesture());
        view.post(new Runnable() {
            @Override
            public void run() {
                doGameInit();
            }
        });
        return view;
    }


    public void doGameInit() {
        bmpKambing = Bitmap.createBitmap(imgGame.getMeasuredWidth(),imgGame.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        cnvKambing = new Canvas(bmpKambing);
        imgGame.setImageBitmap(bmpKambing);
        bmpAnswer = Bitmap.createBitmap(imgAnswer.getWidth(),imgAnswer.getHeight(),Bitmap.Config.ARGB_8888);
        cnvAnswer = new Canvas(bmpAnswer);
        imgAnswer.setImageBitmap(bmpAnswer);

        gameEngine.setColorChoices( new int[]{
                getResources().getColor(R.color.randomColor1),
                getResources().getColor(R.color.randomColor2),
                getResources().getColor(R.color.randomColor3),
                getResources().getColor(R.color.randomColor4),
                getResources().getColor(R.color.randomColor5),
                getResources().getColor(R.color.randomColor6),
        });

        gameEngine.setGameSetting(imgGame.getMeasuredHeight(), imgGame.getMeasuredWidth());
        //trigger update score
        this.onGameTicks(GameTicksEvent.SCORE);
        gameEngine.gameBoardReady();
    }

    @Override
    public void onClick(View v) {
        gameEngine.triggerGameStop();
    }

    public void onGameTicks(GameTicksEvent ticksEvent) {
        Log.d(LOGTAG, "Game ticks!");
        if(ticksEvent == GameTicksEvent.OBSTACLES){
            Log.d(LOGTAG, "Game ticks mode: Obstacles.");
            Log.d(LOGTAG, "Printing " + gameEngine.getObstacles().length + " obstacles.");
            // pasang obstacle baru.
            resetCanvas();
            for(Obstacle ob:gameEngine.getObstacles()) {
                Log.d(LOGTAG, ob.toString());
                ob.drawToImageView(cnvKambing);
            }
            Log.d(LOGTAG, "Finished rendering.");
            imgGame.invalidate();

            //set challengenya apa
            this.showTheAnswer();
        } else if (ticksEvent == GameTicksEvent.NYAWA) {
            int nyawa = gameEngine.getNyawa();
            String textNyawa = "";
            while (nyawa-->0){
                textNyawa += "❤";
            }
            lblNyawa.setText(textNyawa);
        } else if (ticksEvent == GameTicksEvent.SCORE){
            lblHigh.setText(String.format("%d",gameEngine.getHighScore()));
            lblScore.setText(String.format("%d",gameEngine.getScore()));
        }
    }

    private void showTheAnswer(){
        Obstacle challenge = gameEngine.getChallengeObstacle();
        Log.d(LOGTAG, "CHALLANGE: " + challenge.toString());
        challenge.drawItInChallengeBox(cnvAnswer);
        imgAnswer.invalidate();
    }

    private void resetCanvas() {
        cnvAnswer.drawColor(getResources().getColor(R.color.colorPrimaryDark));
        //draw clear reference: https://stackoverflow.com/a/19963143/4721245
        cnvKambing.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
    }

    private class SimpleGesture extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            super.onSingleTapUp(e);
            Log.d(LOGTAG, "Image touched");
            if(gameEngine.getGameStatus() == GameStatus.RUNNING) {
                Log.d(LOGTAG, "Game has started, so we give the game engine an info.");
                gameEngine.onTouched(e.getX(), e.getY());
            }

            return true;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        gesture.onTouchEvent(event);
        return true;
    }

}
