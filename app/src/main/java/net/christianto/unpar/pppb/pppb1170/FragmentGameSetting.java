package net.christianto.unpar.pppb.pppb1170;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import net.christianto.unpar.pppb.pppb1170.Engine.GachaRandomizer;
import net.christianto.unpar.pppb.pppb1170.Engine.GameEngine;
import net.christianto.unpar.pppb.pppb1170.Engine.Internal.RandomString;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGameSetting extends Fragment implements View.OnClickListener{

    private GameEngine gameEngine;
    private GameContext gameContext;
    RadioButton optEasy, optIntermediate, optHard, optUnsolvable;
    Switch swcRandomColor;
    Switch swcRandomShape;
    EditText txtRandText;
    EditText txtRandColor;

    Button btnSave;


    public static FragmentGameSetting createInstance(GameEngine engine, GameContext gameContext) {
        FragmentGameSetting korban = new FragmentGameSetting();
        korban.gameEngine = engine;
        korban.gameContext = gameContext;
        return korban;
    }

    public FragmentGameSetting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View kambing =  inflater.inflate(R.layout.fragment_fragment_game_setting, container, false);

        optEasy = kambing.findViewById(R.id.rBtn_easy);
        optIntermediate = kambing.findViewById(R.id.rBtn_intermediate);
        optHard = kambing.findViewById(R.id.rBtn_hard);
        optUnsolvable = kambing.findViewById(R.id.rBtn_unsolveable);

        swcRandomColor = kambing.findViewById(R.id.switch_random_color);
        swcRandomShape = kambing.findViewById(R.id.switch_random_shape);
        txtRandColor = kambing.findViewById(R.id.et_colorRandomTotal);
        txtRandText = kambing.findViewById(R.id.et_textRandomTotal);

        //set some settings.
        // cara barbar ae, biar cephet.
        txtRandText.setText(String.format("%01d", GachaRandomizer.textCombi));
        txtRandColor.setText(String.format("%01d", GachaRandomizer.colorCombi));

        swcRandomColor.setChecked(GachaRandomizer.randomizeColor);
        swcRandomShape.setChecked(GachaRandomizer.randomizeShape);

        if(GachaRandomizer.quality == GachaRandomizer.TextRandomizeQuality.EASY) {
            optEasy.setChecked(true);
        } else if (GachaRandomizer.quality == GachaRandomizer.TextRandomizeQuality.INTERMEDIATE){
            optIntermediate.setChecked(true);
        } else if (GachaRandomizer.quality == GachaRandomizer.TextRandomizeQuality.HARD){
            optHard.setChecked(true);
        } else if (GachaRandomizer.quality == GachaRandomizer.TextRandomizeQuality.UNSOLVEABLE){
            optUnsolvable.setChecked(true);
        } else {
            optEasy.setChecked(true);
        }

        btnSave = kambing.findViewById(R.id.btn_setGameSetting);
        btnSave.setOnClickListener(this);

        return kambing;
    }


    @Override
    public void onClick(View v) {
        //combi
        GachaRandomizer.colorCombi = Integer.parseInt(txtRandColor.getText().toString());
        GachaRandomizer.textCombi = Integer.parseInt(txtRandText.getText().toString());

        //switchi
        GachaRandomizer.randomizeColor = swcRandomColor.isChecked();
        GachaRandomizer.randomizeShape = swcRandomShape.isChecked();

        //rotten me
        if (optEasy.isChecked()) {
            GachaRandomizer.quality = GachaRandomizer.TextRandomizeQuality.EASY;
        } else if (optIntermediate.isChecked()) {
            GachaRandomizer.quality = GachaRandomizer.TextRandomizeQuality.INTERMEDIATE;
        } else if (optHard.isChecked()) {
            GachaRandomizer.quality = GachaRandomizer.TextRandomizeQuality.HARD;
        } else if (optUnsolvable.isChecked()) {
            GachaRandomizer.quality = GachaRandomizer.TextRandomizeQuality.UNSOLVEABLE;
        } else {
            GachaRandomizer.quality = GachaRandomizer.TextRandomizeQuality.EASY;
        }

        Toast.makeText(this.getContext(), "Settings Saved!", Toast.LENGTH_SHORT).show();
        gameContext.enterMainMenu();
    }
}
