package net.christianto.unpar.pppb.pppb1170.Engine.Models;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.Log;

/**
 * Obstable is counted as simple box
 */
public class Obstacle {
    private static final String LOGTAG = "Obstacler";
    protected int color;
    protected String text;
    protected float posX;
    protected float posY;
    protected RectF boundary;
    protected int padding = 20;
    private int padding_top = 40;
    protected TextPaint tp;
    protected Rect textBounder;



    private boolean isTheCorrectOne = false;

    public Obstacle(int color, String text, float posX, float posY){
        this.setColor(color);
        tp = new TextPaint();
        tp.setColor(Color.WHITE);
        tp.setTextSize(20);
        tp.setAntiAlias(true);
        textBounder = new Rect();
        this.setText(text);
        this.setPosX(posX);
        this.setPosY(posY);
    }

    public Obstacle(int color, String text, Point position){
        this(color, text, position.x, position.y);
    }

    public void setAnswer(boolean isThisTheAnswer) {
        this.isTheCorrectOne = isThisTheAnswer;
    }

    public boolean getAnswer() {
        return isTheCorrectOne;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        updateBoundary();
    }

    protected void updateBoundary() {
        tp.getTextBounds(this.text, 0, text.length(), textBounder);
        float totalWidth = textBounder.width() + padding*2;
        float totalHeight = textBounder.height() + padding_top*2;

        boundary = new RectF(posX, posY, posX + totalWidth, posY + totalHeight);
    }

    public boolean isReallyIsTouched (float touchX, float touchY) {
        // https://stackoverflow.com/q/48477058/4721245
        // return boundary.contains(touchX, touchY);

        if(touchX > boundary.right || touchX < boundary.left) {
            return false;
        }

        if(touchY > boundary.bottom || touchY < boundary.top){
            return false;
        }
        return true;
    }

    public void drawToImageView(Canvas cnv){
        Paint akatsuki = new Paint();
        akatsuki.setAntiAlias(true);
        akatsuki.setColor(color);
        cnv.drawRect(boundary, akatsuki);

        //tp.setColor(Color.CYAN);
        cnv.drawText(text, posX + padding, posY + padding_top + textBounder.height(), tp);
    }

    public void drawItInChallengeBox(Canvas cnv) {
        Paint akatsuki = new Paint();
        akatsuki.setAntiAlias(true);
        akatsuki.setColor(color);

        float left = (cnv.getWidth()-boundary.width())/2,
              top  = (cnv.getHeight()-boundary.height())/2;
        float right  = left + boundary.width(),
              bottom = top + boundary.height();
        RectF bg = new RectF(left, top, right, bottom);

        cnv.drawRect(bg, akatsuki);
        cnv.drawText(text, left + padding, top + padding_top + textBounder.height(), tp);

        Log.d(LOGTAG, "Drawing challenge on " + bg.toShortString());
    }

    public void drawToImageView(Canvas cnv,float posX,float posY){
        Paint akatsuki = new Paint();
        akatsuki.setAntiAlias(true);
        akatsuki.setColor(color);
        RectF rect = new RectF(posX,posY,posX+textBounder.width()+padding*2,posY+textBounder.height()+padding*2 );
        cnv.drawRect(rect, akatsuki);
        cnv.drawText(text, posX + padding, posY + padding + textBounder.height(), tp);
    }

    /**
     * Represent this obstacle as a box. So we can
     * check if it's colliding with each other or not.
     *
     * @return Rectangle in float precision
     */
    public RectF getObstacleAsRectangle(){
        Log.d(LOGTAG, "Boundary obstacle requested: " + boundary.toString());
        return boundary;
    }

    public boolean isColliding(Obstacle otherObstacle) {
        this.updateBoundary();
        otherObstacle.updateBoundary();
        return this.getObstacleAsRectangle().intersect(otherObstacle.getObstacleAsRectangle());
    }

    public void setPos(int x, int y){
        this.setPosY(y);
        this.setPosX(x);
    }

    public float getPosX() {
        return posX;
    }

    public void setPosX(float posX) {
        this.posX = posX;
        updateBoundary();
    }

    public float getPosY() {
        return posY;
    }

    public void setPosY(float posY) {
        this.posY = posY;
        updateBoundary();
    }

    public void setPos(Point point) {
        this.setPos(point.x, point.y);
    }

    public String toString(){
        return String.format("Obstacle({ left:%01f, top:%01f, right:%01f, bottom:%01f, color: %08x, text:%s })", boundary.left, boundary.top, boundary.right, boundary.bottom, color, text);
    }

    public static class Groups {
        public Obstacle[] obstacles;
        public Obstacle challenge;
    }
}
