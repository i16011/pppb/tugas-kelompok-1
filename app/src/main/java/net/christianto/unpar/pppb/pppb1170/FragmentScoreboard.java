package net.christianto.unpar.pppb.pppb1170;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import net.christianto.unpar.pppb.pppb1170.Engine.GameBoard;
import net.christianto.unpar.pppb.pppb1170.Engine.GameEngine;
import net.christianto.unpar.pppb.pppb1170.Helper.ScoreAdapter;

import java.util.List;


public class FragmentScoreboard extends Fragment implements View.OnClickListener{
    GameEngine gameEngine;
    GameContext gameContext;

    Button btnMainMenu;
    ListView lstScores;
    ScoreAdapter sa;

    public FragmentScoreboard() {
        // Required empty public constructor
    }
    public static FragmentScoreboard createInstance(GameEngine engine, GameContext gameContext) {
        FragmentScoreboard korban = new FragmentScoreboard();
        korban.gameEngine = engine;
        korban.gameContext = gameContext;
        return korban;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View kambing = inflater.inflate(R.layout.fragment_fragment_scoreboard, container, false);
        btnMainMenu = kambing.findViewById(R.id.btnMainMenu);
        btnMainMenu.setOnClickListener(this);

        lstScores = kambing.findViewById(R.id.lstScores);

        sa = ScoreAdapter.createInstance(inflater, gameEngine.getScores());

        lstScores.setAdapter(sa);

        return kambing;
    }

    @Override
    public void onClick(View v) {
        gameContext.enterMainMenu();

    }
}
