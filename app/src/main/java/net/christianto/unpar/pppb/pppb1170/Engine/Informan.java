package net.christianto.unpar.pppb.pppb1170.Engine;

import android.content.Context;
import android.util.JsonWriter;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * Will handle such data:
 * Highscore,
 * Score
 *
 * In a file / hopefully internet.
 */
public class Informan {
    private static final String LOGTAG = "Informan";
    private final Context context;
    private long highscore = 0;
    private long score = 0;

    private static Informan instance;

    private String saveFilePath;

    private ArrayList<Long> scores;

    private Informan(String loadFromPath, Context context) {
        this.saveFilePath = loadFromPath;
        this.context = context;

        this.scores = new ArrayList<>();
    }

    /**
     * Load the info from files.
     */
    public void loadIt() {
        // Referensi kenapa nggak butuh Context: https://stackoverflow.com/a/9855725/4721245
        try {
            InputStream input = new FileInputStream(context.getFileStreamPath(saveFilePath));
            BufferedReader in = new BufferedReader(new InputStreamReader(input));

            StringBuilder sb = new StringBuilder();
            String temp = "";
            while ((temp = in.readLine()) != null) {
                sb.append(temp).append('\n');
            }
            in.close();

            JSONObject kambing = new JSONObject(sb.toString());
            JSONArray scores = kambing.getJSONArray("scores");
            for(int i=0; i<scores.length(); i++){
                this.scores.add(scores.getLong(i));
                highscore = Math.max(this.scores.get(i), highscore);
            }
        } catch (IOException e){
            Log.e(LOGTAG, "Error on reading save file.");
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(LOGTAG, "Error on parsing the json array.");
            e.printStackTrace();
        }
    }

    /**
     * Load the info from save folder.
     */
    private void saveIt() {
        try {
            FileWriter writer = new FileWriter(context.getFileStreamPath(saveFilePath));
            JSONObject saveGame = new JSONObject();
            JSONArray scores = new JSONArray();

            Iterator<Long> itt = this.scores.iterator();
            while(itt.hasNext()){
                scores.put(itt.next());
            }

            saveGame.put("scores", scores);
            writer.append(saveGame.toString());
            writer.flush();

            writer.close();
            //done.
        } catch (IOException e) {
            Log.e(LOGTAG, "Error on writing save file.");
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(LOGTAG, "Error on generating save file.");
            e.printStackTrace();
        }

    }


    public static Informan getInstance(Context appContext) {
        if(Informan.instance == null) {
            Informan.instance = new Informan("gamedata.json", appContext);
        }
        Informan.instance.loadIt();

        return Informan.instance;
    }

    public long getHighscore() {
        return highscore;
    }

    public void setHighscore(long highscore) {
        this.highscore = highscore;
    }

    public long getScore() {
        return score;
    }

    public ArrayList<Long> getScores() {
        return scores;
    }

    public void setScore(long score) {
        this.score = score;
    }

    /**
     * Bumps up the score to a certain size.
     * @param size a positive number
     */
    public void bumpsUpScore(int size) {
        this.score += size;
    }

    public void resetScore(){
        this.score = 0;
    }

    public void setScoreEnd() {
        scores.add(score);
        this.highscore = Math.max(this.score, this.highscore);

        Collections.sort(this.scores);
        this.saveIt();
    }
}
